package kindergarden.service;

import kindergarden.dao.PersistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Service
public class NurseService {

    @Autowired
    private PersistanceService persistanceService;

    public List<Object[]> getIllnessBetweenDates(Date from, Date to) {
        Query query = persistanceService.getEntityManager()
                .createQuery(
                        "select a.child.id, a.child.name, a.child.surName, a.child.groupName, count(a) from AttendanceTableRow a where a.skipReason='ILL' and a.date between :from AND :to group by a.child.id");
        query.setParameter("from", from);
        query.setParameter("to", to);
        List<Object[]> list = query.getResultList();

        return list;
    }

    public int getTodayChildrenCount(Date today) {
        Query query = persistanceService.getEntityManager().createQuery("select count(a) from AttendanceTableRow a where a.skipReason is null and a.date=:today");
        query.setParameter("today", today);
        return query.getFirstResult();
    }

}
