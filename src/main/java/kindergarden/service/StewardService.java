package kindergarden.service;

import kindergarden.dao.PersistanceService;
import kindergarden.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Service
public class StewardService {

    @Autowired
    private PersistanceService persistanceService;

    public List<Product> getAllProducts(Date date) {

        Query query = persistanceService.getEntityManager().createQuery("select p from Product p where date=:date");
        query.setParameter("date", date, TemporalType.DATE);
        return query.getResultList();
    }

    @Transactional
    public void saveProducts(List<Product> table, List<Product> tomorrowTable) {

        for (Product product : tomorrowTable) {
            persistanceService.getEntityManager().merge(product);
        }

        for (Product product : table) {
            persistanceService.getEntityManager().merge(product);
        }
    }

    public Product getByDateAndName(Date date, String name) {

        Query query = persistanceService.getEntityManager().createQuery("select p from Product p where p.date=:date and p.name=:name");
        query.setParameter("date", date);
        query.setParameter("name", name);
        return (Product) query.getResultList().get(0);
    }
}
