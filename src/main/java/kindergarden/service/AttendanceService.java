package kindergarden.service;

import kindergarden.dao.PersistanceService;
import kindergarden.model.AttendanceTableRow;
import kindergarden.model.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Service
public class AttendanceService {

    @Autowired
    private PersistanceService persistanceService;

    public List<AttendanceTableRow> getAttendanceBetweenDates(String group, Date from, Date to) {
        String groupClause = "";
        if (!StringUtils.isEmpty(group)) {
            groupClause = " and a.child.groupName='" + group + "'";
        }
        Query query = persistanceService.getEntityManager()
                .createQuery("select a from AttendanceTableRow a where a.date between :from AND :to" + groupClause);
        query.setParameter("from", from);
        query.setParameter("to", to);
        List<AttendanceTableRow> list = query.getResultList();

        return list;
    }

    public List<Child> getAllChildren() {
        List<Child> children = (List<Child>) persistanceService.getAll(Child.class);

        return children;
    }

    @Transactional
    public boolean updateAttendanceTable(List<AttendanceTableRow> table) {

        for (AttendanceTableRow tableRow : table) {
            updateAttendanceRow(tableRow);
        }
        persistanceService.getEntityManager().flush();
        return true;
    }

    public Child getChildById(long id) {
        return (Child) persistanceService.getById(Child.class, id);
    }

    public boolean updateAttendanceRow(AttendanceTableRow attendanceRow) {
        return persistanceService.getEntityManager().merge(attendanceRow) != null;
    }
    public List<Child> getGroupChildren(String group) {
        Query query = persistanceService.getEntityManager()
                .createQuery("select c from Child c where c.groupName='" + group + "'");
        List<Child> list = query.getResultList();

        return list;
    }
}
