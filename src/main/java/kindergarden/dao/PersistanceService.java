package kindergarden.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PersistanceService {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {

        this.entityManager = entityManager;
    }

    public List<?> getAll(Class clazz) {

        return entityManager.createQuery("Select t from " + clazz.getSimpleName() + " t", clazz).getResultList();
    }

    public Object getById(Class clazz, long id) {

        return entityManager.createQuery("Select t from " + clazz.getSimpleName() + " t where t.id=" + id, clazz).getResultList().get(0);
    }
}
