package kindergarden.util;

public interface Constants {

	final int COUNT_ON_PAGE = 4;
	final String USER_NOT_FOUND = "Username not found";
	final String WRONG_PASS = "Wrong password";

}
