package kindergarden.util;

import kindergarden.model.Roles;
import kindergarden.model.SystemUser;
import kindergarden.model.UserRole;
import kindergarden.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserService userService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();
		String password = (String) authentication.getCredentials();

		SystemUser systemUser = new SystemUser();
//        TODO
        UserRole userRole = new UserRole(Roles.ADMIN);
        systemUser.setUserRole(userRole);

		if (systemUser == null) {
			throw new BadCredentialsException(Constants.USER_NOT_FOUND);
		}

		if (!password.equals(systemUser.getPassword())) {
			throw new BadCredentialsException(Constants.WRONG_PASS);
		}

		Collection<UserRole> authorities = new ArrayList<UserRole>();

		authorities.add(systemUser.getUserRole());

		return new UsernamePasswordAuthenticationToken(username, password, authorities);
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}
}