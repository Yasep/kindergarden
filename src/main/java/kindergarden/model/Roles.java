package kindergarden.model;

/**
 * Created by Женя on 07.12.2014.
 */
public enum Roles {
    ADMIN,
    EDUCATOR,
    STEWARD,
    NURSE
}