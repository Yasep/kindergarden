package kindergarden.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class UserRole extends BaseEntity implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Enumerated(EnumType.STRING)
    private Roles role;

    private String note;

    public UserRole() {
    }

    public UserRole(Roles role) {
        this.role = role;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public String getAuthority() {
        return role.name();
    }

    public void setAuthority(String authority) {
        this.role = Roles.valueOf(authority);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}