package kindergarden.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by Женя on 04.12.2014.
 */
@Entity
public class Child extends BaseEntity {

    private String name;

    private String surName;

    private Boolean diet;

    @Enumerated(EnumType.STRING)
    private Groups groupName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Boolean isDiet() {
        return diet;
    }

    public void setDiet(Boolean diet) {
        this.diet = diet;
    }

    public Groups getGroupName() {
        return groupName;
    }

    public void setGroupName(Groups groupName) {
        this.groupName = groupName;
    }
}
