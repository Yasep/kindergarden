package kindergarden.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Женя on 06.12.2014.
 */
//@Entity
public class AttendanceTable extends BaseEntity {

    private List<AttendanceTableRow> rows;

    private Date date;

    public List<AttendanceTableRow> getRows() {
        return rows;
    }

    public void setRows(List<AttendanceTableRow> rows) {
        this.rows = rows;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
