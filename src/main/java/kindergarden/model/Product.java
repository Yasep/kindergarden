package kindergarden.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Женя on 04.12.2014.
 */
@Entity
@Table(name = "product", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"date", "name"})
})
public class Product extends BaseEntity {

    private String name;

    private float afterCount;

    private float minus;

    private float plus;

    private float beforeCount;

    @Temporal(value = TemporalType.DATE)
    private Date date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMinus() {
        return minus;
    }

    public void setMinus(float minus) {
        this.minus = minus;
    }

    public float getPlus() {
        return plus;
    }

    public void setPlus(float plus) {
        this.plus = plus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getBeforeCount() {
        return beforeCount;
    }

    public void setBeforeCount(float beforeCount) {
        this.beforeCount = beforeCount;
    }

    public float getAfterCount() {
        return afterCount;
    }

    public void setAfterCount(float afterCount) {
        this.afterCount = afterCount;
    }
}