package kindergarden.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Женя on 06.12.2014.
 */
@Entity
public class AttendanceTableRow extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "child_id")
    private Child child;

    private String skipReason;

    @Temporal(value = TemporalType.DATE)
    private Date date;

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSkipReason() {
        return skipReason;
    }

    public void setSkipReason(String skipReason) {
        this.skipReason = skipReason;
    }
}
