package kindergarden.dto;

import java.util.List;

/**
 * Created by Женя on 06.12.2014.
 */
public class AttendanceTableDTO {

    private List<AttendanceTableRowDTO> rows;


    public List<AttendanceTableRowDTO> getRows() {
        return rows;
    }

    public void setRows(List<AttendanceTableRowDTO> rows) {
        this.rows = rows;
    }

}
