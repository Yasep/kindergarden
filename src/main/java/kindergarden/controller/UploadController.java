package kindergarden.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class UploadController {

	@RequestMapping(value = "/uploadForm/{fileType}/{containerId}")
	@Secured({ "ROLE_ADMIN", "ROLE_EMPLOYEE" })
	public String getUploadForm(@PathVariable("fileType") String fileType, @PathVariable("containerId") String containerId,
			Map<String, Object> model) {
		model.put("fileType", fileType);
		model.put("containerId", containerId);
		return "uploadForm";
	}

}
