package kindergarden.controller;

import kindergarden.dto.AttendanceTableRowDTO;
import kindergarden.model.AttendanceTableRow;
import kindergarden.model.Child;
import kindergarden.model.SkipReason;
import kindergarden.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/attendance")
public class AttendanceController {

    @Autowired
    private AttendanceService attendanceService;

    @RequestMapping(value = "/table", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<AttendanceTableRowDTO> getAttendanceTable(
            @RequestParam(value = "group", required = false) String group) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        Date from = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date to = calendar.getTime();

        List<AttendanceTableRow> table = attendanceService.getAttendanceBetweenDates(group, from, to);

        return getTableRows(table, group);
    }

    @RequestMapping(value = "/table/save", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public HttpStatus updateTable(@RequestBody List<AttendanceTableRowDTO> table) {

        List<AttendanceTableRow> attendances = getAttendancesFromTable(table);

        if (attendanceService.updateAttendanceTable(attendances)) {
            return HttpStatus.OK;
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @RequestMapping(value = "/tableRow/save", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public HttpStatus updateTableRow(@RequestBody AttendanceTableRowDTO rowDTO) {

        List<AttendanceTableRow> attendanceRows = getAttendanceRowFromDTO(rowDTO);

        if (attendanceService.updateAttendanceTable(attendanceRows)) {
            return HttpStatus.OK;
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    private List<AttendanceTableRow> getAttendancesFromTable(List<AttendanceTableRowDTO> tableDTO) {
        List<AttendanceTableRow> table = new ArrayList<>();

        for (AttendanceTableRowDTO tableRowDTO : tableDTO) {
            List<AttendanceTableRow> tableRows = getAttendanceRowFromDTO(tableRowDTO);
            if (tableRows != null && !tableRows.isEmpty()) {
                table.addAll(tableRows);
            }
        }
        return table;
    }

    private List<AttendanceTableRow> getAttendanceRowFromDTO(AttendanceTableRowDTO tableRowDTO) {
        Child child = attendanceService.getChildById(tableRowDTO.getId());
        if (child != null) {
            List<AttendanceTableRow> tableRows = new ArrayList<>();
            for (Field field : tableRowDTO.getClass().getDeclaredFields()) {

                try {
                    field.setAccessible(true);
                    String value = String.valueOf(field.get(tableRowDTO));
                    if (value != null &&
                            (value.equals(SkipReason.ILL.name()) || value.equals(SkipReason.DOMESTIC.name()))) {

                        AttendanceTableRow tableRow = new AttendanceTableRow();
                        tableRow.setSkipReason(value);
                        tableRow.setChild(child);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(new Date());
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(field.getName().substring(1)));
                        tableRow.setDate(calendar.getTime());

                        tableRows.add(tableRow);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return tableRows;
        }
        return null;
    }

    private List<AttendanceTableRowDTO> getTableRows(List<AttendanceTableRow> table, String group) {
        List<AttendanceTableRowDTO> tableRows = new ArrayList<>();
        List<Child> children = null;
        if (StringUtils.isEmpty(group)) {
            children = attendanceService.getAllChildren();
        } else {
            children = attendanceService.getGroupChildren(group);
        }

        for (Child child : children) {

            AttendanceTableRowDTO tableRowDTO = new AttendanceTableRowDTO();
            tableRowDTO.setId(child.getId());
            tableRowDTO.setName(child.getName() + " " + child.getSurName());
            for (AttendanceTableRow tableRow : table) {

                if (tableRow.getChild().getId() == child.getId()) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(tableRow.getDate());
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    for (Field field : tableRowDTO.getClass().getDeclaredFields()) {
                        if (field.getName().substring(1).equals(String.valueOf(day))) {
                            field.setAccessible(true);
                            try {
                                if (tableRow.getSkipReason() != null) {
                                    field.set(tableRowDTO, tableRow.getSkipReason());
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            if (tableRowDTO != null) {
                tableRows.add(tableRowDTO);
            }
        }
        return tableRows;
    }

}
