package kindergarden.controller;

import kindergarden.model.Product;
import kindergarden.service.StewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/products")
public class StewardController {

    @Autowired
    private StewardService stewardService;

    @RequestMapping(value = "/table", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Product> getPlusMinusTable(@RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {

        if (date == null) {
            date = new Date();
        }

        List<Product> table = stewardService.getAllProducts(date);

        for (Product product : table) {
            product.setDate(new Date(product.getDate().getTime()));
        }
        return table;
    }

    @RequestMapping(value = "/table/save", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public HttpStatus savePlusMinusTable(@RequestBody List<Product> table) {

        if (table != null && !table.isEmpty()) {
            List<Product> tomorrowTable = new ArrayList<>();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(table.get(0).getDate());
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) + 1);

            for (Product product : table) {
                product.setAfterCount(product.getBeforeCount() - product.getMinus() + product.getPlus());

                Product tomorrowProduct = stewardService.getByDateAndName(calendar.getTime(), product.getName());
                if (tomorrowProduct == null) {
                    tomorrowProduct = new Product();
                }
                tomorrowProduct.setName(product.getName());
                tomorrowProduct.setBeforeCount(product.getAfterCount());
                tomorrowProduct.setAfterCount(product.getAfterCount());
                tomorrowProduct.setDate(calendar.getTime());
                tomorrowTable.add(tomorrowProduct);
            }
            stewardService.saveProducts(table, tomorrowTable);
        }
        return HttpStatus.OK;
    }

}
