package kindergarden.controller;

import kindergarden.dao.PersistanceService;
import kindergarden.dto.UserDto;
import kindergarden.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class UserController {

    @Autowired
    private PersistanceService persistanceService;

    @RequestMapping(value = "/login", consumes = "application/json")
    public ResponseEntity<UserDto> login(@RequestBody UserDto userDto) {

        if (!StringUtils.isEmpty(userDto.getUsername()) && !StringUtils.isEmpty(userDto.getPassword())) {
            SystemUser user = (SystemUser) persistanceService.getEntityManager()
                    .createQuery("select u from SystemUser u where u.userName like '" + userDto.getUsername() +
                            "' and u.password like '" + userDto.getPassword() + "'").getSingleResult();
            if (user != null) {
                userDto.setId(user.getId());
                userDto.setNote(user.getUserRole().getNote());
                userDto.setRole(user.getUserRole().getAuthority());
                return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
            }
        }
        return new ResponseEntity<UserDto>(userDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
