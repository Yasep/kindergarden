Ext.define('App.controller.StewardController', {
    extend: 'Ext.app.Controller',
    models: ['App.model.Product'],
    stores: ['App.store.ProductStore'],
    views: ['App.Viewport', 'App.view.StewardView'],
    refs: [
        {
            ref: 'print',
            selector: 'stewards button[action = print]'
        },
        {
            ref: 'plusMinus',
            selector: 'stewards grid[id=plusMinus]'
        },
        {
            ref: 'orderGrid',
            selector: 'stewards grid[id=order]'
        }
        ,
        {
            ref: 'date',
            selector: 'stewards datefield[id=date]'
        }

    ],
    init: function () {

        this.control(
            {
                'stewards button[action = printpm]': {click: this.printpm},
                'stewards button[action = loadOnDate]': {click: this.initPlusMinus},
                'stewards button[action = savepm]': {click: this.savepm},
                'stewards grid[id=order]': {edit: this.onEdit, boxready: this.initOrder},
                'stewards grid[id=plusMinus]': {edit: this.onEdit, boxready: this.initPlusMinus}
            });
    },

    initPlusMinus: function () {
        var me = this,
            date = me.getDate().getValue(),
            store = me.getPlusMinus().getStore();
        if (!Ext.isEmpty(date)) {
            Ext.apply(store.getProxy().extraParams, {
                date: date
            });
        }
        store.reload();
    },

    initOrder: function () {
        var me = this,
            store = me.getOrderGrid().getStore();
        store.reload();
    },

    savepm: function () {
        debugger
        this.getPlusMinus().getStore().sync({success: this.onSuccessSave(), failure: this.onFailureSave()});
    },

    onEdit: function () {
        var record = arguments[1].record,
            row = record.getData();
        record.set('afterCount', row.beforeCount - row.minus + row.plus)
    },

    printpm: function () {
        Ext.ux.grid.Printer.printAutomatically = false;
        Ext.ux.grid.Printer.mainTitle = 'Книга учета';
        Ext.ux.grid.Printer.print(this.getPlusMinus());
    },

    onSuccessSave: function () {
    },
    onFailureSave: function () {
    }



});