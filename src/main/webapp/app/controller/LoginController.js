Ext.define('App.controller.LoginController', {
    extend: 'Ext.app.Controller',
    views: ['App.Viewport'],

    refs: [
        {
            ref: 'viewport',
            selector: 'viewport'
        },
        {
            ref: 'first',
            selector: 'panel[name=first]'
        },
        {
            ref: 'mainView',
            selector: 'mainView'
        },
        {
            ref: 'mainTab',
            selector: 'tabpanel[id=mainTab]'
        },
        {
            ref: 'atTable',
            selector: 'atTable'
        },
        {
            ref: 'atGrid',
            selector: 'atTable grid'
        },
        {
            ref: 'illGrid',
            selector: 'nurses grid'
        },
        {
            ref: 'plusMinus',
            selector: 'stewards grid[id = plusMinus]'
        },
        {
            ref: 'nurses',
            selector: 'nurses'
        },
        {
            ref: 'loginForm',
            selector: 'form'
        },
        {
            ref: 'logoutButton',
            selector: 'mainView button[id=logout]'
        },
        {
            ref: 'loginButton',
            selector: 'loginform button[action=login]'
        }
    ],

    init: function () {
        this.control({
            'mainView button[action=logout]': {
                click: function (button) {
                    var me = this;

                    me.getFirst().getLayout().setActiveItem(0);
                    me.getMainTab().removeAll();
                }
            },

            'loginform button[action=login]': {
                click: function (button) {
                    var me = this,
                        loginButton = this.getLoginButton();

                    Ext.Ajax.request({
                        url: 'login/login',
                        method: 'POST',
                        success: function (response) {
                            var user = Ext.decode(response.responseText)
                            me.activateView(user);
                        },
                        failure: function () {
                            Ext.MessageBox.alert('Ошибка', "Неправильный пароль или логин");
                        },
                        jsonData: me.getLoginForm().form.getValues()
                    });
                }
            }
        });
    },

    activateView: function (user) {
        var me = this;
        switch (user.role) {
            case 'ADMIN':
                me.activateAdminView();
                break;
            case 'EDUCATOR':
                me.activateEducatorView(user.note);
                break;
            case 'NURSE':
                me.activateNurseView();
                break;
            case 'STEWARD':
                me.activateStewardView();
                break;
            default :
                Ext.MessageBox.alert('Ошибка', "Неправильный пароль или логин");
        }
    },

    activateAdminView: function () {
        var me = this;

        me.getFirst().getLayout().setActiveItem(1);

        var atTable = Ext.create('widget.atTable', {title: 'ТАБЕЛЬ ПОСЕЩЕНИЙ'}),
            nurses = Ext.create('widget.nurses', {title: 'КАБИНЕТ МЕДСЕСТРЫ'}),
            stewards = Ext.create('widget.stewards', {title: 'КАБИНЕТ ЗАВХОЗА'});

        me.getMainTab().add(atTable);
        me.getMainTab().add(nurses);
        me.getMainTab().add(stewards);
        me.getMainTab().setActiveTab(0);
    },

    activateNurseView: function () {
        var me = this;

        me.getFirst().getLayout().setActiveItem(1);
        var view = Ext.create('widget.nurses', {title: 'КАБИНЕТ МЕДСЕСТРЫ'});

        me.getMainTab().add(view);
        me.getMainTab().setActiveTab(0);
    },

    activateStewardView: function () {
        var me = this;

        me.getFirst().getLayout().setActiveItem(1);
        var view = Ext.create('widget.stewards', {title: 'КАБИНЕТ ЗАВХОЗА'});

        me.getMainTab().add(view);
        me.getMainTab().setActiveTab(0);
    },

    activateEducatorView: function (group) {
        var me = this;

        me.getFirst().getLayout().setActiveItem(1);
        var view = Ext.create('widget.atTable', {title: 'ТАБЕЛЬ ПОСЕЩЕНИЙ'}),
            store = me.getAtGrid().getStore();

        Ext.apply(store.getProxy().extraParams, {
            group: group
        });
        me.getMainTab().add(view);
        me.getMainTab().setActiveTab(0);
    }

});