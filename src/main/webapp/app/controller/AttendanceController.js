Ext.define('App.controller.AttendanceController', {
    extend: 'Ext.app.Controller',
    models: ['App.model.AttRow'],
    stores: ['App.store.AttRowStore'],
    views: ['App.Viewport', 'App.view.AttendanceTable'],
    refs: [
        {
            ref: 'viewport',
            selector: 'viewport'
        },
        {
            ref: 'saveButton',
            selector: 'atTable button[action = save]'
        },
        {
            ref: 'atGrid',
            selector: 'atTable grid'
        }


    ],
    init: function () {

        this.control(
            {
                'atTable grid': {boxready: this.initAtTable},
                'atTable button[action = print]': {click: this.print},
                'atTable button[action = save]': {click: this.save}
            });
    },

    initAtTable: function () {
        this.getAtGrid().getStore().load();
    },

    save: function () {
        this.getAtGrid().getStore().sync({success: this.onSuccessSave(), failure: this.onFailureSave()});
    },

    print: function () {
        Ext.ux.grid.Printer.printAutomatically = false;
        Ext.ux.grid.Printer.mainTitle = 'Табель посещаемости';
        Ext.ux.grid.Printer.print(this.getAtGrid());
    },

    onSuccessSave: function () {
    },
    onFailureSave: function () {
    }

});