Ext.define('App.controller.NurseController', {
    extend: 'Ext.app.Controller',
    models: ['App.model.IllnessRow'],
    stores: ['App.store.IllnessRowStore'],
    views: ['App.Viewport', 'App.view.NursesView'],
    refs: [
        {
            ref: 'viewport',
            selector: 'viewport'
        },
        {
            ref: 'print',
            selector: 'nurses button[action = print]'
        },
        {
            ref: 'illnessGrid',
            selector: 'nurses grid[id=illness]'
        },
        {
            ref: 'from',
            selector: 'nurses datefield[id=from]'
        },
        {
            ref: 'to',
            selector: 'nurses datefield[id=to]'
        },
        {
            ref: 'orderGrid',
            selector: 'nurses grid[id=order]'
        }


    ],
    init: function () {

        this.control(
            {
                'nurses grid[id=illness]': {boxready: this.loadIllness},
                'nurses grid[id=order]': {boxready: this.loadOrder},
                'nurses button[action = print]': {click: this.print},
                'nurses button[action = illness]': {click: this.loadIllness},
                'nurses button[action = save]': {click: this.save}
            });
    },

    loadIllness: function () {
        var me = this,
            store = me.getIllnessGrid().getStore();
        Ext.apply(store.getProxy().extraParams, {
            from: me.getFrom().getValue(),
            to: me.getTo().getValue()
        });
        store.reload();
    },

    loadOrder: function () {
        var me = this,
            store = me.getOrderGrid().getStore();
        store.reload();
    },

    save: function () {

        this.getOrderGrid().getStore().sync();
    },

    print: function () {
        Ext.ux.grid.Printer.printAutomatically = false;
        Ext.ux.grid.Printer.mainTitle = 'Табель заболеваемости';
        Ext.ux.grid.Printer.print(this.getIllnessGrid());
    }


});