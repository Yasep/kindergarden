Ext.define('App.store.UserStore', {
    extend: 'Ext.data.Store',
    requires: 'App.model.User',
    model: 'App.model.User',
//    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'login/login',
        method: 'GET',
        reader: {
            type: 'json'
        }
    }

});