Ext.ns('Model.Product.fields');

Model.Product.fields.id = 'id';
Model.Product.fields.name = 'name';
Model.Product.fields.count = 'afterCount';
Model.Product.fields.minus = 'minus';
Model.Product.fields.plus = 'plus';
Model.Product.fields.before = 'beforeCount';
Model.Product.fields.date = 'date';

Ext.define('App.model.Product', {
    extend: 'Ext.data.Model',
    fields: [
        {name: Model.Product.fields.id},
        {name: Model.Product.fields.name, type: 'string'},
        {name: Model.Product.fields.count, type: 'float'},
        {name: Model.Product.fields.minus, type: 'float'},
        {name: Model.Product.fields.plus, type: 'float'},
        {name: Model.Product.fields.before, type: 'float'},
        {name: Model.Product.fields.date, type: 'float'}
    ]
});