Ext.ns('Model.IllnessRow.fields');

Model.IllnessRow.fields.id = 'id';
Model.IllnessRow.fields.name = 'name';
Model.IllnessRow.fields.count = 'count';
Model.IllnessRow.fields.group = 'group';

Ext.define('App.model.IllnessRow', {
    extend: 'Ext.data.Model',
    fields: [
        {name: Model.IllnessRow.fields.id},
        {name: Model.IllnessRow.fields.name, type: 'string'},
        {name: Model.IllnessRow.fields.group, type: 'string'},
        {name: Model.IllnessRow.fields.count, type: 'int'}
    ]
});