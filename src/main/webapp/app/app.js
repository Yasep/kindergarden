/**
 * Created by Женя on 01.09.2014.
 */

Ext.application({
    name: 'App',
    appFolder: 'app',
    controllers: [
        'App.controller.AttendanceController',
        'App.controller.LoginController',
        'App.controller.StewardController',
        'App.controller.NurseController'
    ],
    requires: [
        'Ext.ux.grid.Printer',
        'App.Viewport',
        'App.view.AttendanceTable',
        'App.view.StewardView',
        'App.view.NursesView',
        'App.view.MainView',
        'App.view.LoginForm'
    ],

    launch: function () {
        Ext.create('App.Viewport', {});
    }
});