/**
 * Created by Женя on 02.09.2014.
 */

Ext.define('App.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'fit',
    cls: 'viewport',
    items: [
        {
            name: 'first',
            xtype: 'panel',
            layout: 'card',
            items: [
                {
//                    title: 'Здравстуйте',
                    layout: {type: 'vbox', align: 'center'},
                    xtype: 'panel',
                    items: {
                        xtype: 'loginform'
                    }
                },
                {
//                    hidden: true,
                    margin: '20',
                    xtype: 'mainView'
                }
            ]
        }

    ]
})
