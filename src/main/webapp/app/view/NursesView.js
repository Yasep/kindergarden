/**
 * Created by Женя on 02.09.2014.
 */
Ext.define('App.view.NursesView', {
        extend: 'Ext.tab.Panel',
        requires: ['App.store.IllnessRowStore'],
        alias: 'widget.nurses',
        cls: 'nurses',
        layout: 'card',
        tbar: [
            { xtype: 'tbspacer', flex: 1 },
            {
                xtype: 'button',
                action: 'print',
                text: 'Печать'
            }
        ],
        items: [
            {
                xtype: 'panel',
                title: 'Табель заболеваемости',
                items: [
                    {
                        xtype: 'grid',
                        id: 'illness',
                        store: Ext.create('App.store.IllnessRowStore'),
                        forceFit: false,
                        title: 'Заболеваемость',
                        columnLines: true,
                        selType: 'cellmodel',
                        border: false,
                        tbar: [
                            { xtype: 'tbspacer', flex: 1 },
                            {
                                xtype: 'button',
                                action: 'illness',
                                text: 'Загрузить'
                            },
                            {
                                xtype: 'datefield',
                                anchor: '100%',
                                labelWidth: 20,
                                fieldLabel: 'С',
                                id: 'from'
                            },
                            {
                                xtype: 'datefield',
                                anchor: '100%',
                                labelWidth: 20,
                                fieldLabel: 'По',
                                id: 'to'
                            }
                        ],
                        columns: {

                            items: [

                                {
                                    text: 'Имя',
                                    dataIndex: Model.IllnessRow.fields.name,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'Группа',
                                    dataIndex: Model.IllnessRow.fields.group,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'Болел дней',
                                    dataIndex: Model.IllnessRow.fields.count,
                                    flex: 1,
                                    sortable: true
                                }

                            ]
                        }
                    }
                ]
            },

            {
                xtype: 'panel',
                title: 'Заказ на сегодня',
                items: [
                    {
                        xtype: 'grid',
                        id: 'order',
                        store: Ext.create('App.store.ProductStore'),
                        forceFit: false,
                        title: 'Продукты',
                        columnLines: true,
                        selType: 'cellmodel',
                        plugins: Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2}),
                        border: false,
                        tbar: [
                            { xtype: 'tbspacer', flex: 1 },
                            {
                                xtype: 'button',
                                action: 'printd',
                                text: 'Печать'
                            }
                        ],
                        columns: {

                            items: [

                                {
                                    text: 'Название',
                                    dataIndex: Model.Product.fields.name,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'Количество',
                                    dataIndex: Model.Product.fields.minus,
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        selectOnTab: true
                                    },
                                    sortable: true
                                }

                            ]
                        }
                    },
                    {
                        xtype: 'panel',
                        align: 'bottom',
                        margin: 20,
                        items: [
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'hbox',
                                    pack: 'end',
                                    defaultMargins: {
                                        right: 10,
                                        left: 10
                                    }
                                },
                                defaults: {
                                    xtype: 'button'
                                },
                                items: [
                                    {
                                        text: 'Сохранить',
                                        action: 'save'
                                    }
                                ]

                            }
                        ]
                    }
                ]
            }

        ]
    }
);

