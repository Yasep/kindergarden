Ext.define('App.view.LoginForm' ,{
    extend: 'Ext.form.FormPanel',
    xtype : 'loginform',

    name: 'loginform',
    frame: true,
    title: 'Авторизация',
    bodyPadding: '5px 5px 0',
    width: 350,
    height: 150,
    fieldDefaults: {
        labelWidth: 125,
        msgTarget: 'side',
        autoFitErrors: false
    },
    defaults: {
        width: 300,
        inputType: 'password'
    },
    defaultType: 'textfield',

    initComponent: function() {
        this.buttons = [
            {
                name: 'loginButton',
                text: 'Login',
                action: 'login'
            }
        ];

        this.items = [
            {
                fieldLabel: 'Username',
                name: 'username',
                id: 'username',
                inputType: 'text'
            },
            {
                fieldLabel: 'Password',
                name: 'password',
                inputType: 'password'
            }
        ];

        this.callParent(arguments);
    }
});