/**
 * Created by Женя on 02.09.2014.
 */

Ext.define('App.view.MainView', {
        extend: 'Ext.panel.Panel',
        xtype: 'mainView',
        cls: 'mainView',
        tbar: [
            { xtype: 'tbspacer', flex: 1},
                    {
                        xtype: 'button',
                        action: 'logout',
                        text: 'Выход'
                    }
        ],
        items: [

            {
                xtype: 'tabpanel',
                id: 'mainTab',
                layout: {type: 'card'}
            }
        ]
    }
)
;

