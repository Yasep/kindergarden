/**
 * Created by Женя on 02.09.2014.
 */
Ext.define('App.view.StewardView', {
        extend: 'Ext.tab.Panel',
        requires: ['App.store.ProductStore'],
        alias: 'widget.stewards',
        cls: 'stewards',
        layout: 'card',
        items: [
            {
                xtype: 'panel',
                title: 'Книга учета',
                items: [
                    {
                        xtype: 'grid',
                        id: 'plusMinus',
                        store: Ext.create('App.store.ProductStore'),
                        forceFit: false,
                        title: 'Продукты',
                        columnLines: true,
                        selType: 'cellmodel',
                        plugins: Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2}),
                        border: false,
                        tbar: [
                            { xtype: 'tbspacer', flex: 1 },

                            {
                                xtype: 'datefield',
                                anchor: '100%',
                                labelWidth: 30,
                                fieldLabel: 'дата',
                                id: 'date'
                            },
                            {
                                xtype: 'button',
                                action: 'loadOnDate',
                                text: 'Загрузить'
                            },
                            {
                                xtype: 'button',
                                action: 'printpm',
                                text: 'Печать'
                            }
                        ],
                        columns: {
                            items: [

                                {
                                    text: 'Наименование',
                                    dataIndex: Model.Product.fields.name,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'на начало дня',
                                    dataIndex: Model.Product.fields.before,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'прибыло',
                                    dataIndex: Model.Product.fields.plus,
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        selectOnTab: true
                                    },
                                    sortable: true
                                },
                                {
                                    text: 'убыло',
                                    dataIndex: Model.Product.fields.minus,
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        selectOnTab: true
                                    },
                                    sortable: true
                                },
                                {
                                    text: 'остаток',
                                    dataIndex: Model.Product.fields.count,
                                    flex: 1,
                                    sortable: true
                                }

                            ]
                        }
                    },
                    {
                        xtype: 'panel',
                        align: 'bottom',
                        margin: 20,
                        items: [
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'hbox',
                                    pack: 'end',
                                    defaultMargins: {
                                        right: 10,
                                        left: 10
                                    }
                                },
                                defaults: {
                                    xtype: 'button'
                                },
                                items: [
                                    {
                                        text: 'Сохранить',
                                        action: 'savepm'
                                    }
                                ]

                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                title: 'Заказ на сегодня',
                items: [
                    {
                        xtype: 'grid',
                        id: 'order',
                        store: Ext.create('App.store.ProductStore'),
                        forceFit: false,
                        title: 'Продукты',
                        columnLines: true,
                        selType: 'cellmodel',
                        plugins: Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2}),
                        border: false,
                        tbar: [
                            { xtype: 'tbspacer', flex: 1 },
                            {
                                xtype: 'button',
                                action: 'printd',
                                text: 'Печать'
                            }
                        ],
                        columns: {

                            items: [

                                {
                                    text: 'Название',
                                    dataIndex: Model.Product.fields.name,
                                    flex: 1,
                                    sortable: true
                                },
                                {
                                    text: 'Количество',
                                    dataIndex: Model.Product.fields.minus,
                                    flex: 1,
                                    sortable: true
                                }

                            ]
                        }
                    },
                    {
                        xtype: 'panel',
                        align: 'bottom',
                        margin: 20,
                        items: [
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'hbox',
                                    pack: 'end',
                                    defaultMargins: {
                                        right: 10,
                                        left: 10
                                    }
                                },
                                defaults: {
                                    xtype: 'button'
                                },
                                items: [
                                    {
                                        text: 'Сохранить',
                                        action: 'saved'
                                    }
                                ]

                            }
                        ]
                    }
                ]
            }
        ]
    }
);

