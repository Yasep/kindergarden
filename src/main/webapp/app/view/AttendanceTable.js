/**
 * Created by Женя on 02.09.2014.
 */
Ext.define('App.view.AttendanceTable', {
        extend: 'Ext.panel.Panel',
        requires: ['App.store.AttRowStore'],
        alias: 'widget.atTable',
        cls: 'atTable',
        layout: 'anchor',
        tbar: [
            { xtype: 'tbspacer', flex: 1 },
            {
                xtype: 'button',
                action: 'print',
                text: 'Печать'
            }
        ],
        items: [
            {
                xtype: 'grid',
                store: Ext.create('App.store.AttRowStore'),
                forceFit: false,
                columnLines: true,
                selType: 'cellmodel',
                border: false,
                plugins: Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2}),
                columns: {

                    defaults: {
                        width: 50,
                        renderer: function (value) {
                            if (value === 'ILL') {
                                return 'Бол';
                            } else if (value === 'DOMESTIC') {
                                return 'Быт';
                            }
                        },
                        editor: {
                            xtype: 'combobox',
                            typeAhead: true,
                            triggerAction: 'all',
                            selectOnTab: true,
                            store: [
                                ['ILL', 'Болезнь'],
                                ['DOMESTIC', 'Бытовая'],
                                ['PRESENT', 'Присутствовал']
                            ],
                            lazyRender: true,
                            listClass: 'x-combo-list-small'
                        }
                    },
                    items: [

                        {
                            text: 'Имя',
                            dataIndex: 'name',
                            width: 200,
                            sortable: true,
                            renderer: function (value) {
                                return value.toUpperCase();
                            },
                            editor: null
                        },
                        {
                            text: '1',
                            dataIndex: 'd1',
                            sortable: true
                        },
                        {
                            text: '2',
                            dataIndex: 'd2',
                            sortable: true
                        },
                        {
                            text: '3',
                            dataIndex: 'd3',
                            sortable: true
                        },
                        {
                            text: '4',
                            dataIndex: 'd4',
                            sortable: true
                        },
                        {
                            text: '5',
                            dataIndex: 'd5',
                            sortable: true
                        },
                        {
                            text: '6',
                            dataIndex: 'd6',
                            sortable: true
                        },
                        {
                            text: '7',
                            dataIndex: 'd7',
                            sortable: true
                        },
                        {
                            text: '8',
                            dataIndex: 'd8',
                            sortable: true
                        },
                        {
                            text: '9',
                            dataIndex: 'd9',
                            sortable: true
                        },
                        {
                            text: '10',
                            dataIndex: 'd10',
                            sortable: true
                        },
                        {
                            text: '11',
                            dataIndex: 'd11',
                            sortable: true
                        },
                        {
                            text: '12',

                            dataIndex: 'd12',
                            sortable: true
                        },
                        {
                            text: '13',
                            dataIndex: 'd13',
                            sortable: true
                        },
                        {
                            text: '14',

                            dataIndex: 'd14',
                            sortable: true
                        },
                        {
                            text: '15',
                            dataIndex: 'd15',
                            sortable: true
                        },
                        {
                            text: '16',
                            dataIndex: 'd16',
                            sortable: true
                        },
                        {
                            text: '17',
                            dataIndex: 'd17',
                            sortable: true
                        },
                        {
                            text: '18',
                            dataIndex: 'd18',
                            sortable: true
                        },
                        {
                            text: '19',

                            dataIndex: 'd19',
                            sortable: true
                        },
                        {
                            text: '20',
                            dataIndex: 'd20',
                            sortable: true
                        },
                        {
                            text: '21',

                            dataIndex: 'd21',
                            sortable: true
                        },
                        {
                            text: '22',
                            dataIndex: 'd22',
                            sortable: true
                        },
                        {
                            text: '23',

                            dataIndex: 'd23',
                            sortable: true
                        },
                        {
                            text: '24',
                            dataIndex: 'd24',
                            sortable: true
                        },
                        {
                            text: '25',
                            dataIndex: 'd25',
                            sortable: true
                        },
                        {
                            text: '26',
                            dataIndex: 'd26',
                            sortable: true
                        },
                        {
                            text: '27',
                            dataIndex: 'd27',
                            sortable: true
                        },
                        {
                            text: '28',

                            dataIndex: 'd28',
                            sortable: true
                        },
                        {
                            text: '29',

                            dataIndex: 'd29',
                            sortable: true
                        },
                        {
                            text: '30',
                            dataIndex: 'd30',
                            sortable: true
                        },
                        {
                            text: '31',
                            dataIndex: 'd31',
                            sortable: true
                        }

                    ]
                }
            },
            {
                xtype: 'panel',
                align: 'bottom',
                margin: 20,
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'end',
                            defaultMargins: {
                                right: 10,
                                left: 10
                            }
                        },
                        defaults: {
                            xtype: 'button'
                        },
                        items: [
                            {
                                text: 'Сохранить',
                                action: 'save'
                            }
                        ]

                    }
                ]
            }
        ]
    }
);

