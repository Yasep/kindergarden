-- --------------------------------------------------------
-- Хост                          :127.0.0.1
-- Версия сервера                :5.5.16 - MySQL Community Server (GPL)
-- ОС Сервера                    :Win32
-- HeidiSQL Версия               :7.0.0.4279
-- Создано                       :2014-12-14 22:20:22
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных kindergarden
CREATE DATABASE IF NOT EXISTS `kindergarden` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kindergarden`;


-- Dumping structure for таблица kindergarden.attendancetablerow
CREATE TABLE IF NOT EXISTS `attendancetablerow` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `skipReason` varchar(255) DEFAULT NULL,
  `child_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `child_id` (`child_id`),
  CONSTRAINT `child_id` FOREIGN KEY (`child_id`) REFERENCES `child` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kindergarden.attendancetablerow: ~34 rows (приблизительно)
/*!40000 ALTER TABLE `attendancetablerow` DISABLE KEYS */;
INSERT INTO `attendancetablerow` (`id`, `date`, `skipReason`, `child_id`) VALUES
	(1, '2014-12-14', NULL, 1),
	(2, '2014-12-09', 'DOMESTIC', 2),
	(3, '2014-12-14', NULL, 3),
	(4, '2014-12-16', 'ILL', 4),
	(5, '2014-12-14', NULL, 5),
	(6, '2014-12-14', NULL, 6),
	(7, '2014-12-08', 'ILL', 7),
	(8, '2014-12-14', NULL, 8),
	(9, '2014-12-14', 'DOMESTIC', 9),
	(10, '2014-12-14', NULL, 10),
	(11, '2014-12-11', 'ILL', 11),
	(12, '2014-12-14', NULL, 12),
	(13, '2014-12-14', NULL, 13),
	(14, '2014-12-14', NULL, 14),
	(15, '2014-12-11', 'ILL', 15),
	(16, '2014-12-14', NULL, 16),
	(17, '2014-12-14', 'DOMESTIC', 17),
	(29, '2014-12-14', NULL, 1),
	(30, '2014-12-12', 'ILL', 2),
	(31, '2014-12-14', NULL, 3),
	(32, '2014-12-14', NULL, 4),
	(33, '2014-12-14', NULL, 5),
	(34, '2014-12-14', NULL, 6),
	(35, '2014-12-14', NULL, 7),
	(36, '2014-12-14', NULL, 8),
	(37, '2014-12-14', NULL, 9),
	(38, '2014-12-09', 'ILL', 10),
	(39, '2014-12-14', NULL, 11),
	(40, '2014-12-14', NULL, 12),
	(41, '2014-12-14', NULL, 13),
	(42, '2014-12-14', 'ILL', 14),
	(43, '2014-12-14', NULL, 15),
	(44, '2014-12-14', 'DOMESTIC', 16),
	(45, '2014-12-14', NULL, 17);
/*!40000 ALTER TABLE `attendancetablerow` ENABLE KEYS */;


-- Dumping structure for таблица kindergarden.child
CREATE TABLE IF NOT EXISTS `child` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `diet` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surName` varchar(255) DEFAULT NULL,
  `groupName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kindergarden.child: ~17 rows (приблизительно)
/*!40000 ALTER TABLE `child` DISABLE KEYS */;
INSERT INTO `child` (`id`, `diet`, `name`, `surName`, `groupName`) VALUES
	(1, NULL, 'Руслан', 'Белый', 'Ясли'),
	(2, NULL, 'Гарик ', 'Мартиросян', 'Ясли'),
	(3, NULL, 'Вадик', 'Галыгин', 'Ясли'),
	(4, NULL, 'Юля', 'Ахметова', 'Ясли'),
	(5, NULL, 'Игорь', 'Ласточкин', 'Ясли'),
	(6, NULL, 'Оля', 'Кортункова', 'Ясли'),
	(7, NULL, 'Пашка', 'Воля', 'Ясли'),
	(8, NULL, 'Гарик', 'Харламов', 'Ясли'),
	(9, NULL, 'Демис', 'Карибидис', 'Старшая'),
	(10, NULL, 'Марина', 'Кравец', 'Старшая'),
	(11, NULL, 'Ира', 'Чеснокова', 'Старшая'),
	(12, NULL, 'Слава', 'Комисаренко', 'Старшая'),
	(13, NULL, 'Лейсан', 'Утяшева', 'Старшая'),
	(14, NULL, 'Гаврил', 'Гордеев', 'Старшая'),
	(15, NULL, 'Слава', 'Комисаренко', 'Старшая'),
	(16, NULL, 'Сашка', 'Незлобин', 'Старшая'),
	(17, NULL, 'Тимур', 'Батрутдинов', 'Старшая');
/*!40000 ALTER TABLE `child` ENABLE KEYS */;


-- Dumping structure for таблица kindergarden.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `afterCount` float NOT NULL,
  `minus` float NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `plus` float NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `beforeCount` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kindergarden.product: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `afterCount`, `minus`, `name`, `plus`, `date`, `beforeCount`) VALUES
	(1, 12, 5, 'Мясо', 3, '2014-12-14', 14),
	(2, 20, 4, 'Гречка', 2, '2014-12-14', 20),
	(3, 7, 0, 'Хлеб', 0, '2014-12-14', 7);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- Dumping structure for таблица kindergarden.systemuser
CREATE TABLE IF NOT EXISTS `systemuser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `secondName` varchar(255) DEFAULT NULL,
  `surName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `user_role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`userName`),
  KEY `FK9D23FEBAE576796E` (`user_role_id`),
  CONSTRAINT `FK9D23FEBAE576796E` FOREIGN KEY (`user_role_id`) REFERENCES `userrole` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kindergarden.systemuser: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `systemuser` DISABLE KEYS */;
INSERT INTO `systemuser` (`id`, `firstName`, `password`, `secondName`, `surName`, `userName`, `user_role_id`) VALUES
	(1, '1', '1', '1', '1', '1', 1),
	(2, '3', '3', '3', '3', '3', 3),
	(3, '2', '2', '2', '2', '2', 2),
	(4, '4', '4', '4', '4', '4', 4);
/*!40000 ALTER TABLE `systemuser` ENABLE KEYS */;


-- Dumping structure for таблица kindergarden.userrole
CREATE TABLE IF NOT EXISTS `userrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `note` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kindergarden.userrole: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` (`id`, `note`, `role`) VALUES
	(1, 'Старшая', 'EDUCATOR'),
	(2, NULL, 'ADMIN'),
	(3, NULL, 'NURSE'),
	(4, NULL, 'STEWARD'),
	(5, 'Ясли', 'EDUCATOR');
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
